﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz4
{
    /// <summary>
    /// Инициализация программы
    /// </summary>
    internal class StartInit
    {
        private readonly IConfiguration configuration;

        public StartInit(IConfiguration conf)
        {
            configuration = conf;
        }

        public void Init()
        {
            Common.ConnectionString = configuration.GetConnectionString("postgre");
            Common.TableWidth = configuration.GetValue<int>("TableWidth");

            Common.Scripts.Add("clients", configuration.GetValue<string>("scripts:clients"));
            Common.Scripts.Add("depozits", configuration.GetValue<string>("scripts:depozits"));
            Common.Scripts.Add("operations", configuration.GetValue<string>("scripts:operations"));
            Common.Scripts.Add("newClient", configuration.GetValue<string>("scripts:newClient"));
        }
    }
}
