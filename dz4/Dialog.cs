﻿using dz4.Models;
using System.Data;

namespace dz4
{
    /// <summary>
    /// Диалоговый блок
    /// </summary>
    internal static class Dialog
    {
        /// <summary>
        /// Меню
        /// </summary>
        /// <returns>символ, веденный с консоли</returns>
        public static char Menu()
        {
            Console.WriteLine("Что вы хотите сделать?");
            Console.WriteLine("1 - просмотреть все таблицы");
            Console.WriteLine("2 - добавить нового клиента");
            Console.WriteLine("q - Выход");
            return Console.ReadKey().KeyChar;
        }

        /// <summary>
        /// Запрос данных нового клиента
        /// </summary>
        /// <returns></returns>
        public static Client NewClientData()
        {
            Client client = new();

            Console.Write("\nВведите Фамилию: ");
            client.LastName = Console.ReadLine()??"";

            Console.Write("Введите имя: ");
            client.FirstName = Console.ReadLine() ?? "";

            return client;
        }

        /// <summary>
        /// Вывод таблицы на экран 
        /// </summary>
        /// <param name="dt">DataTable для вывода</param>
        /// <param name="name">Наименование таблицы</param>
        public static void TablePrint(DataTable dt, string name)
        {
            //Определим количество столбцов
            var coloums = dt.Columns.Count;

            //Разделительная строка
            string line = new('_', 20 * coloums + coloums + 1);

            Console.WriteLine($"\n{name}:");
            Console.WriteLine($"{line,20}");

            for (var i = 0; i < coloums; i++) //Выводим заголовки столбцов
            {
                Console.Write($"|{dt.Columns[i].ColumnName,20}");
            }
            Console.Write("|\n");
            Console.WriteLine($"|{line.Remove(line.Length - 2),20}|");
            foreach (DataRow row in dt.Rows)
            {
                for (var i = 0; i < coloums; i++)
                {
                    Console.Write($"|{row[i],20}");
                }
                Console.Write("|\n");
            }
            Console.WriteLine($"|{line.Remove(line.Length - 2),20}|\n");
        }
    }
}
