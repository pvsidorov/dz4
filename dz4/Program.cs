﻿using dz4.Data;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Data;

namespace dz4
{
    internal class Program
    {
        /// <summary>
        /// Главный метод
        /// </summary>
        /// <returns></returns>
        static async Task Main()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json")
                .AddJsonFile($"scripts.json");
            var config = builder.Build();

            (new StartInit(config)).Init(); //Инициализируем программы

            var Data = new DataHandler(config.GetConnectionString("postgre")); //Устанавливаем контекст БД
            
            bool cicle = true;
            while (cicle)
            {
                var key = Dialog.Menu();
                switch (key)
                {
                    case 'q': //Выход
                        cicle = false;
                        break;
                    case '1': //Выводим таблицы
                        Dialog.TablePrint(await Data.GetDataAsync(Common.Scripts["clients"]), "Клиенты");
                        Dialog.TablePrint(await Data.GetDataAsync(Common.Scripts["depozits"]), "Депозиты");
                        Dialog.TablePrint(await Data.GetDataAsync(Common.Scripts["operations"]), "Операции");
                        break;
                    case '2': //Создадим клиента, после чего отобразим спиок клиентов
                        var client = Dialog.NewClientData();
                        var result = await Data.AddDataAsync(Common.Scripts["newClient"], client.ParametrsAddPrepare());
                        if (result == 0)
                            Console.WriteLine("Не удалось добавить новую запись");
                        else
                            Dialog.TablePrint(await Data.GetDataAsync(Common.Scripts["clients"]), "Клиенты");
                        break;
                    default: continue;
                }
            }
        }
    }
}