﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz4
{
    /// <summary>
    /// Настройки программы
    /// </summary>
    public static class Common
    {
        public static string ConnectionString { get; set; }

        public static Dictionary<string, string> Scripts { get; set; }= new Dictionary<string, string>();
    }
}
