﻿using System.Data;

namespace dz4.Data
{
    public interface IDataHandler
    {
        Task<int> AddDataAsync(string cmd, Dictionary<string, object> param);
        Task<DataTable> GetDataAsync(string cmd);
    }
}