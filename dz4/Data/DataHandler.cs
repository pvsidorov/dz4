﻿using Npgsql;
using System.Data;

namespace dz4.Data
{
    /// <summary>
    /// Работа с данными
    /// </summary>
    public class DataHandler : IDataHandler
    {
        private readonly string connectionString;

        public DataHandler(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Запрос таблицы с базы
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public async Task<DataTable> GetDataAsync(string cmd)
        {
            await using var conn = new NpgsqlConnection(connectionString);
            await conn.OpenAsync();
            await using var _cmd = new NpgsqlCommand(cmd, conn);
            var reader = await _cmd.ExecuteReaderAsync();
            var tb = new DataTable();
            tb.Load(reader);
            await conn.CloseAsync();
            return tb;
        }

        /// <summary>
        /// Вставка данных в базу
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<int> AddDataAsync(string cmd, Dictionary<string, object> param)
        {
            await using var conn = new NpgsqlConnection(connectionString);
            await conn.OpenAsync();
            await using var _cmd = new NpgsqlCommand(cmd, conn);

            foreach (var item in param)
            {
                _cmd.Parameters.Add(new NpgsqlParameter(item.Key, item.Value));
            }
            await _cmd.PrepareAsync();
            var result = await _cmd.ExecuteNonQueryAsync();
            await conn.CloseAsync();
            return result;
        }
    }
}
