﻿using dz4.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz4.Models
{
    internal class Client
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Подготовка параметров для передачи в запрос
        /// </summary>
        /// <returns></returns>
        public Dictionary<string,object> ParametrsAddPrepare()
        {
            var result = new Dictionary<string, object>
            {
                { "lastname", LastName },
                { "firstname", FirstName }
            };
            return result;
        }

    }
}
